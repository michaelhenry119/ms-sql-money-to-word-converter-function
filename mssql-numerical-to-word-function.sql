--CREATE : Michael Henry Pantaleon
CREATE Function [dbo].[fnOnes]
(
	@iOnes CHAR(1)
)
	RETURNS VARCHAR(10)
	AS BEGIN
	Declare @sOutput VARCHAR(10)
	SET @sOutput = CASE @iOnes
		WHEN '1' THEN 'One ' 
		WHEN '2' THEN 'Two ' 
		WHEN '3' THEN 'Three ' 
		WHEN '4' THEN 'Four '
		WHEN '5' THEN 'Five ' 
		WHEN '6' THEN 'Six '
		WHEN '7' THEN 'Seven '
		WHEN '8' THEN 'Eight '
		WHEN '9' THEN 'Nine '
		WHEN '0' THEN ''
	END
	RETURN @sOutput
END

--CREATE : Michael Henry Pantaleon
CREATE Function [dbo].[fnTens]
(
	@iTens CHAR(2)
)
	RETURNS VARCHAR(20)
	AS BEGIN
	Declare @sOutput VARCHAR(20)
 
	SET @sOutput = CASE SUBSTRING(@iTens,1,1)
		WHEN '1' THEN CASE SUBSTRING(@iTens,2,1)
				WHEN '0' THEN 'Ten '
				WHEN '1' THEN 'Eleven '
				WHEN '2' THEN 'Twelve '
				WHEN '3' THEN 'Thirteen '
				WHEN '4' THEN 'Fourteen ' 
				WHEN '5' THEN 'Fifteen '
				WHEN '6' THEN 'Sixteen ' 
				WHEN '7' THEN 'Seventeen '
				WHEN '8' THEN 'Eighteen '
				WHEN '9' THEN 'Nineteen '
			END
		WHEN '2' THEN 'Twenty ' + dbo.fnOnes(SUBSTRING(@iTens,2,1))
		WHEN '3' THEN 'Thirty ' +  dbo.fnOnes(SUBSTRING(@iTens,2,1))
		WHEN '4' THEN 'Forty ' + dbo.fnOnes(SUBSTRING(@iTens,2,1))
		WHEN '5' THEN 'Fifty ' +  dbo.fnOnes(SUBSTRING(@iTens,2,1))
		WHEN '6' THEN 'Sixty ' + dbo.fnOnes(SUBSTRING(@iTens,2,1))
		WHEN '7' THEN 'Seventy ' +  dbo.fnOnes(SUBSTRING(@iTens,2,1))
		WHEN '8' THEN 'Eighty ' + dbo.fnOnes(SUBSTRING(@iTens,2,1))
		WHEN '9' THEN 'Ninety ' +  dbo.fnOnes(SUBSTRING(@iTens,2,1))
		WHEN '0' THEN dbo.fnOnes(SUBSTRING(@iTens,2,1))
	    END
		
	RETURN @sOutput	
END

--CREATE : Michael Henry Pantaleon
CREATE FUNCTION [dbo].[fnHundreds] 
( 
	@iHundreds CHAR(3)
)
RETURNS VARCHAR(100)
AS 
BEGIN
	DECLARE @sOutput VARCHAR(1000)
 
		SET @sOutput = CASE LEN(CAST(CAST(@iHundreds AS INT) AS VARCHAR))
			WHEN 0 THEN ''
			WHEN 1 THEN dbo.fnOnes(@iHundreds)
			WHEN 2 THEN dbo.fnTens(@iHundreds)
			WHEN 3 THEN dbo.fnOnes(SUBSTRING(@iHundreds,1,1)) + 'Hundred ' + dbo.fnTens(SUBSTRING(@iHundreds,2,2)) 
				 
			 					
		END

RETURN @sOutput
END


--CREATE : Michael Henry Pantaleon
CREATE FUNCTION [dbo].[fnMoneyToWordConverter]
(
	@sTest VARCHAR(20)
)	
RETURNS VARCHAR(8000)
AS
BEGIN

DECLARE @sOutput VARCHAR(8000)= ''
DECLARE @iCounter INT =0
DECLARE @iDigitPlace INT
DECLARE @DigitPlaceWord VARCHAR(10)

DECLARE @WholePart VARCHAR(15)
DECLARE @FloatingPart VARCHAR(100)
DECLARE @FloatingPoint VARCHAR(10)

	IF CHARINDEX('.',@sTest) > 0 
		BEGIN
			SET @FloatingPoint = 'And ' 
			SET @WholePart = SUBSTRING(@sTest,1,CHARINDEX('.',@sTest )-1)
			DECLARE @FloatingPartDot VARCHAR(10) =CONVERT(VARCHAR,ROUND(CONVERT(FLOAT, '0' + SUBSTRING(@sTest,CHARINDEX('.',@sTest ),LEN(@sTest)-CHARINDEX('.',@sTest )+ 1)),2))
			SET @FloatingPart = CASE  LEN(SUBSTRING(@FloatingPartDot,3,LEN(@FloatingPartDot)-2)) 
				WHEN 1   THEN  dbo.fnTens(SUBSTRING(@FloatingPartDot,3,LEN(@FloatingPartDot)-2) + '0') + 'centavos'
				WHEN 2 THEN dbo.fnTens(SUBSTRING(@FloatingPartDot,3,LEN(@FloatingPartDot)-2) ) + 'centavos'
				END 
			
		END
	ELSE
		BEGIN
			SET @FloatingPoint = ''
			SET @FloatingPart = ''	
			SET @WholePart = @sTest
		END

WHILE @iCounter < LEN(@WholePart) 
BEGIN
	SET @iDigitPlace = @iCounter/3
	SET @DigitPlaceWord = CASE @iDigitPlace
			WHEN 0 THEN ''
			WHEN 1 THEN 'Thousand '
			WHEN 2 THEN 'Million '
			WHEN 3 THEN 'Billion ' 
			WHEN 4 THEN 'Trillion ' 
		END
 	
	IF (@iCounter%3=0)
		BEGIN
			SET @sOutput = dbo.fnHundreds(REVERSE(SUBSTRING(REVERSE(@WholePart),@iCounter + 1,3))) + @DigitPlaceWord + @sOutput  
			--PRINT @sOutput
		END
SET @iCounter = @iCounter +1
END
--PRINT 'output : ' + @sOutput
RETURN @sOutput + @FloatingPoint + 	 @FloatingPart  
END
